



REQUIREMENTS
------------

 PHP >= 5.6

INSTALLATION
------------

Git clone https://bitbucket.org/remindbg/articleblog.git 

cd articleblog

composer install 

dbdetails /config/db.php

php yii migrate:fresh 

php yii serve -p 8000
