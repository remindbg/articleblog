<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Tags;
use yii\helpers\ArrayHelper;
use app\models\ArticleTag;
use app\Models\ArticleWithTags;
/* @var $this yii\web\View */
/* @var $model app\Models\Articles */
/* @var $form yii\widgets\ActiveForm */

$tags = $model->tags;


?>

<div class="articles-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'tags')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Tags::find()->all(),'id','name'),
        'value' => $tags, // set the initial display text
        'options' => ['placeholder' => 'Select a Tag', 'multiple' => true],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [',', ' '],
            'maximumInputLength' => 5
        ],
    ])->label('Tag Multiple'); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
