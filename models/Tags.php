<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "tags".
 *
 * @property int $id
 * @property string $name
 *
 * @property ArticleTag[] $articleTags
 */
class Tags extends \yii\db\ActiveRecord
{
    public $articleTags = [];
    /**
     * {@inheritdoc}
     */

    public static function tableName()
    {
        return 'tags';
    }

    public static function getAvailableTags()
    {
        $tags = self::find()->order('name')->asArray()->all();
        $items = ArrayHelper::map($tags, 'id', 'name');
        return $items;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function getName() {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getArticles()
    {
        return $this->hasMany(Articles::className(), ['id' => 'article_id'])->viaTable(ArticleTag::tableName(),['tag_id','id']);
    }



    public function getArticleTags()
    {
        return $this->hasMany(ArticleTag::className(), ['tag_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return TagsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TagsQuery(get_called_class());
    }
}
