<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "articles".
 *
 * @property int $id
 * @property string $title
 * @property string $body
 * @property string $created_at
 * @property tags[] $tags
 * @property ArticleTag[] $articleTags
 */

class Articles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */


    //public $tags = []; no need


    public static function tableName()
    {
        return 'articles';
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['body'], 'string'],
            [['created_at'], 'safe'],
            [['title'], 'string', 'max' => 100],
            [['body'], 'string', 'max' => 500],
            [['tags'], 'safe'],

        ];
    }

    public function setTags($value) //just in case to avoid  setter assign error
    {
        $this->tags = $value;
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'created_at' => 'Created At',

        ];
    }

    public function beforeSave($insert) // assign a date on Insert
    {
        if (parent::beforeSave($insert)) {
            if($insert)
                $this->created_at = date('Y-m-d H:i:s');
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags(){
        return $this->hasMany( Tags::class,['id'=>'tag_id'])->viaTable(ArticleTag::tableName(),['article_id'=>'id']);
    }
    public function getArticleTag()
    {
        //return $this->hasMany(ArticleTag::class, ['article_id' => 'id']);

        return $this->hasMany(ArticleTag::className(), ['article_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ArticleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArticleQuery(get_called_class());
    }
}
