<?php

namespace app\Models;

use yii\helpers\ArrayHelper;

class ArticleWithTags extends Articles
{

    /**
     * @var array IDs of the categories
     */
        public  $tag_ids = [];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            // each category_id must exist in category table (*1)
            ['tag_ids', 'each', 'rule' => [
                'exist', 'targetClass' => Tags::className(), 'targetAttribute' => 'id'
            ]
            ],
        ]);
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'tag_ids' => 'tags',
        ]);
    }

    /**
     * load tag_ids , todo finally works
     */
    public function loadTags()
    {
        $this->tag_ids = [];
        if (!empty($this->id)) {
            $rows = ArticleTag::find()
                ->select(['tag_id'])->distinct()
                ->where(['article_id' => $this->id])
                ->asArray()->all();
            // ffs to fix
            // TODO names
            foreach($rows as $row) {
                $this->tag_ids[] = $row['tag_id'];

            }
        }
    }

    /**
     * save the article w tags
     */
    public function saveTags()
    {
        /* clear the tags of the article before saving */
        ArticleTag::deleteAll(['article_id' => $this->id]);
        if (is_array($this->tag_ids)) {
            foreach($this->tag_ids as $tag_id) {
                $pc = new ArticleTag();
                $pc->article_id = $this->id;
                $pc->tag_id = $tag_id;
                $pc->save();
            }
        }

    }
}