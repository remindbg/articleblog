<?php

namespace app\controllers;

use app\models\ArticleTag;
use Yii;
use app\Models\Articles;
use app\Models\ArticlesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
/**
 * ArticlesController implements the CRUD actions for Articles model.
 */
class ArticlesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Articles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticlesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Articles model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Articles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Articles();

        if ($model->load(Yii::$app->request->post()) && $model->save() ) {
            if($model->validate()) {

                $data= $model->tags;

                if(is_array($data)) { //fix if there are no tags
                    foreach ($data as $value) { // assign tags from $data array
                        //print_r($value);
                        $articleTag = new ArticleTag();
                        $articleTag->article_id = $model->id;
                        $articleTag->tag_id = $value;
                        $articleTag->save();
                    }
                }

                //echo '<pre>', var_dump($data), '</pre>'; die();



            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Articles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = Articles::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           if ($model->validate()) {

               $data = $model->tags;

               if(is_array($data)) {
                   // WOW, TODO better fix
                   ArticleTag::deleteAll(['article_id' => $model->id]); // TODO - clear old records before start assigning

                   foreach ($data as $value) {
                       $articleTag = new ArticleTag();
                       $articleTag->article_id = $model->id;
                       $articleTag->tag_id = $value;
                       $articleTag->save();
                   }
               }


           }

            Yii::$app->getSession()->setFlash('success', 'Article Edited');
           return $this->redirect(['view', 'id' => $model->id]);

       }

        return $this->render('update', [
            'model' => $model,


        ]);
    }


    /**
     * Deletes an existing Articles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        // onDeleteCascade
        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('danger', 'Article Deleted');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Articles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Articles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Articles::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
