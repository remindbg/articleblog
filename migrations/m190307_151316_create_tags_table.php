<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tags`.
 */
class m190307_151316_create_tags_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tags', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),

        ],'CHARACTER set utf8 ENGINE InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tags');
    }
}
