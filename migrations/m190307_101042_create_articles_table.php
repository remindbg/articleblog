<?php

use yii\db\Migration;




/**
 * Handles the creation of table `articles`.
 */
class m190307_101042_create_articles_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('articles', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'body' => $this->text()->null(),
            'created_at' => $this->dateTime()->notNull()


        ],'CHARACTER set utf8 ENGINE InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('articles');
    }
}
