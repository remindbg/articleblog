<?php

use yii\db\Migration;

/**
 * Handles the creation of table `article_tag`.
 */
class m190307_152039_create_article_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('article_tag', [


            'id' => $this->primaryKey(),
            'article_id' => $this->integer()->notNull(),
            'tag_id' => $this->integer()->notNull(),

        ],'CHARACTER set utf8 ENGINE InnoDB');

        // doing the relation with foreign keys

        $this->addForeignKey(
            'fk_article',
            'article_tag',
            'article_id',
            'articles',
            'id',
            'CASCADE'
        );

        // add foreign key for  `tag`
        $this->addForeignKey(
            'fk_tag',
            'article_tag',
            'tag_id',
            'tags',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('article_tag');
    }
}
